<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Lukáš Lalinský - Tag: bazaar</title>
		<description>Random notes and stuff</description>
		<link>http://oxygene.sk</link>
		<language>en</language>
		
		<item>
			<title>Bazaar commit metadata</title>
			<link>http://oxygene.sk/2009/11/bazaar-commit-metadata/</link>
			<comments>http://oxygene.sk/2009/11/bazaar-commit-metadata/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I maintain a few open source projects that use SVN, so notes like &quot;Fixes bug #123, patch by J. Random Hacker&quot; in commit messages are more than usual. When I started using Bazaar for Picard, I thought it would be nice to handle these natively. Bazaar could store bug metadata since &lt;a href=&quot;http://doc.bazaar-vcs.org/bzr.2.0/en/release-notes/bzr-0.16rc1.html&quot;&gt;version 0.16&lt;/a&gt;, using the &lt;code&gt;bzr commit --fixes&lt;/code&gt; option, so that was nice. It kind of inspired me to add the other part, the author name, which was a little more important for me than bug numbers. I wanted contributors who send plain patches to be equally credited for their work in the default branch viewing tools. I knew that Git had the concept of separated &quot;committer&quot; and &quot;change author&quot; and I really liked the idea, so I submitted a patch to Bazaar to add something similar (that was in &lt;a href=&quot;http://doc.bazaar-vcs.org/bzr.2.0/en/release-notes/bzr-0.91rc1.html&quot;&gt;bzr 0.91&lt;/a&gt;). The change allows you specify the author name on commit, that would be stored along in the revision along with the committer name. So you can run a command like this:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;bzr commit --author &quot;J. Random Hacker &amp;lt;jr@example.com&amp;gt;&quot; --fixes project:123 -m &quot;Blah, blah, ...&quot;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;And then see the author name in other tools like &lt;code&gt;bzr log&lt;/code&gt; or &lt;code&gt;bzr annotate&lt;/code&gt;:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;------------------------------------------------------------
revno: 1
author: J. Random Hacker &amp;lt;jr@example.com&amp;gt;
committer: Lukáš Lalinský &amp;lt;lalinsky@gmail.com&amp;gt;
[...]
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Naturally, commit, log and annotate from QBzr also supported this since the day I wrote the patch. It&#39;s a shame that &lt;code&gt;bzr log&lt;/code&gt; only displays author names, not the bug information, because that makes the useful feature quite hidden if you are not using any GUI plugin. I think QBzr users tend to use these features more often, because the commit dialog make it very visible that there is a possibility to do so, but also because &lt;code&gt;bzr qlog&lt;/code&gt; will then nicely present the metadata (labels in the revision graph, clickable links, search for bug numbers, etc.). Here you can see an example with revision that fixes two bugs and the committer is different from the change author:&lt;/p&gt;

&lt;p&gt;&lt;a href=&quot;/uploads/qlog-picard.png&quot;&gt;&lt;img src=&quot;/uploads/qlog-picard.png&quot; alt=&quot;qlog-picard&quot; /&gt;&lt;/a&gt;&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=164</guid>
			
			<pubDate>Mon, 16 Nov 2009 19:26:04 -0000</pubDate>
		</item>
		
		<item>
			<title>Working with branches in Bazaar</title>
			<link>http://oxygene.sk/2009/10/working-with-branches-in-bazaar/</link>
			<comments>http://oxygene.sk/2009/10/working-with-branches-in-bazaar/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;Many people dislike the directory-per-branch concept that &lt;a href=&quot;http://bazaar-vcs.org/&quot;&gt;Bazaar&lt;/a&gt; uses. What they don&#39;t realize though, is that this doesn&#39;t mean you need to have a working tree for each branch. You can very easily simulate cheap Git-style branches, but with some added flexibility. Checkouts are a fairly well known feature of Bazaar, but people mostly associate it with the centralized workflow (i.e. checking out remote branches). This is not the only use case for them.&lt;/p&gt;

&lt;p&gt;When I work on larger projects, where I need multiple branches, I usually have a directory structure like this:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&quot;project&quot;

&lt;ul&gt;
&lt;li&gt;&quot;branches&quot;

&lt;ul&gt;
&lt;li&gt;&quot;branchA&quot;&lt;/li&gt;
&lt;li&gt;&quot;branchB&quot;&lt;/li&gt;
&lt;li&gt;&quot;trunk&quot;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&quot;work&quot;&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;In this example, &quot;project&quot; is a &lt;a href=&quot;http://doc.bazaar-vcs.org/bzr.2.0/en/user-reference/bzr_man.html#repositories&quot;&gt;shared repository&lt;/a&gt;. It contains revisions for project&#39;s branches on a single place. The repository is created with the &lt;code&gt;--no-trees&lt;/code&gt; option, so that working trees are not automatically for new branches. All the branches I need to work with are located in &quot;project/branches/XXX&quot;. Thanks to the DAG model, they represent nothing more than pointers to the &quot;head&quot; revision in the repository, so they are pretty cheap to create.&lt;/p&gt;

&lt;p&gt;My development happens in &quot;project/work&quot;, which is a &lt;a href=&quot;http://doc.bazaar-vcs.org/bzr.2.0/en/user-reference/bzr_man.html#checkouts&quot;&gt;lightweight checkout&lt;/a&gt; to one of the branches. This means that it doesn&#39;t contain anything else but information about the state of the working tree and a pointer to the branch. For any operation, Bazaar will use the branch it points to instead.&lt;/p&gt;

&lt;p&gt;I&#39;ll use QBzr as an example how to set this up:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;% bzr init-repo --no-trees qbzr
Shared repository (format: 2a)
Location:
shared repository: qbzr
% cd qbzr
% mkdir branches
% bzr branch lp:qbzr branches/trunk
Branched 1032 revision(s).
% bzr branch lp:qbzr/0.14 branches/0.14
Branches 969 revisions(s).
% bzr co --lightweight branches/trunk work
% cd work
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;After doing this, I can work commit/pull/push in the &quot;work&quot; directory as if I was in the &quot;trunk&quot; branch. Nothing exciting. Let&#39;s say I want to fix a bug in the &quot;0.14&quot; branch:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;% bzr switch ../branches/0.14
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Now I can work as if I was in the &quot;0.14&quot; branch. So I do some changes, commit them, do some more changes and realize that these should actually go to a new feature branch. So I&#39;ll not commit them and create the new branch (I use this very often, so I have &lt;code&gt;branch --switch&lt;/code&gt; aliased to &lt;code&gt;sbranch&lt;/code&gt;):&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;% bzr branch --switch ../branches/0.14 ../branches/new-feature
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;At this point the &quot;work&quot; directory points to the the &quot;new-feature&quot; branch and the uncommitted changes are still there. So I can commit them, do some more work, merge from other branches, etc. While working on something, I might want to run code from two branches at the same time for comparison. This is where Git doesn&#39;t help you, because you can have only &lt;em&gt;one&lt;/em&gt; working tree at a time (unless you make a new clone of the repository). But with this layout in Bazaar, nothing says I can only have one checkout in the repository. I can actually have a checkout of one of the branches anywhere on the disk. So I do this:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;% cd ..
% bzr co --lightweight ../branches/trunk tmp
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;And now I can run both versions from &quot;work&quot; and &quot;tmp&quot; side-by-side. After I&#39;m done, I simply delete the &quot;tmp&quot; directory.&lt;/p&gt;

&lt;p&gt;I&#39;m writing this mostly because I&#39;m surprised how little people know about it and I personally find it a very nice way to work in Bazaar.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=82</guid>
			
			<pubDate>Thu, 29 Oct 2009 14:53:58 -0000</pubDate>
		</item>
		
	</channel>
</rss>

