<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Lukáš Lalinský - Tag: database</title>
		<description>Random notes and stuff</description>
		<link>http://oxygene.sk</link>
		<language>en</language>
		
		<item>
			<title>Extending the SQLAlchemy SQL expression language</title>
			<link>http://oxygene.sk/2012/09/extending-the-sqlalchemy-sql-expression-language/</link>
			<comments>http://oxygene.sk/2012/09/extending-the-sqlalchemy-sql-expression-language/#disqus_thread</comments>
			
			<category>programming</category>
			
			<description>&lt;p&gt;The SQL expression language from SQLAlchemy is already very flexible and
allows you to build almost any standard SQL query, but sometimes you just
need to use a SQL extension that isn&#39;t supported by SQLAlchemy.&lt;/p&gt;

&lt;p&gt;Suppose I have a very simple music datababse and I want a list of all artists
with their latest album name. Oracle has special aggregate functions
&lt;a href=&quot;http://docs.oracle.com/cd/B19306_01/server.102/b14200/functions056.htm&quot;&gt;&lt;code&gt;FIRST&lt;/code&gt;/&lt;code&gt;LAST&lt;/code&gt;&lt;/a&gt;, which together with the &lt;code&gt;KEEP&lt;/code&gt; clause can help with queries like
this, but unfortunately it isn&#39;t supported by the Oracle dialect in SQLAlchemy.
The SQL query would look like this:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;SELECT
    artist,
    MIN(name) KEEP (DENSE_RANK LAST ORDER BY release_date)
FROM album
GROUP BY artist
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Recently, I&#39;ve learned that it&#39;s possible to extend the &lt;a href=&quot;http://docs.sqlalchemy.org/en/rel_0_7/core/compiler.html&quot;&gt;SQL compiler&lt;/a&gt; and
add support for custom clauses that SQLAlchemy doesn&#39;t understand natively.
In my specific case of the &lt;code&gt;KEEP&lt;/code&gt; clause, I need this code to make it work:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;import itertools
from sqlalchemy.util import to_list
from sqlalchemy.sql.expression import ColumnElement, ClauseList
from sqlalchemy.ext.compiler import compiles

class Keep(ColumnElement):

    def __init__(self, func, order_by, first=True):
        super(Keep, self).__init__()
        self.func = func
        self.order_by = ClauseList(*to_list(order_by))
        self.first = first

    @property
    def type(self):
        return self.func.type

def keep_first(func, order_by):
    return Keep(func, order_by)

def keep_last(func, order_by):
    return Keep(func, order_by, first=False)

@compiles(Keep)
def compile_keep(keep, compiler, **kwargs):
    return &quot;%s KEEP (DENSE_RANK %s ORDER BY %s)&quot; % (
        compiler.process(keep.func),
        &quot;FIRST&quot; if keep.first else &quot;LAST&quot;,
        compiler.process(keep.order_by)
    )
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;With this, I could simply use &lt;code&gt;keep_first()&lt;/code&gt; and &lt;code&gt;keep_last()&lt;/code&gt; as any
other SQL expression functions. For example, the above SQL query would
have been written like this:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;session.query(Album.artist,
              keep_last(sql.func.min(Album.name), Album.release_date)).\
    group_by(Album.artist)
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;The main advantage of having it all written as a SQL expression is that
I don&#39;t need to know the actual column names, which get more and more
complicated once you have some joins and subqueries in the SQL expression.&lt;/p&gt;
</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2012/09/extending-the-sqlalchemy-sql-expression-language/</guid>
			
			<pubDate>Tue, 25 Sep 2012 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Acoustid database dump available</title>
			<link>http://oxygene.sk/2011/01/acoustid-database-dump-available/</link>
			<comments>http://oxygene.sk/2011/01/acoustid-database-dump-available/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;I&#39;ve finally written a script to take a consistent dump of the Acoustid database in the PostgreSQL tab-separated format used by the &lt;a href=&quot;http://www.postgresql.org/docs/8.4/static/sql-copy.html&quot;&gt;COPY&lt;/a&gt; command. I do not have any tools for importing it into PostgreSQL, so it has to be done manually by running SQL commands, but if anybody is interested in playing with the database, you can download it &lt;a href=&quot;http://acoustid.org/data/&quot;&gt;here&lt;/a&gt; (2.7G after compression using bzip2, 6.4G uncompressed). The data is licensed under a &lt;a href=&quot;http://creativecommons.org/licenses/by-sa/3.0/&quot;&gt;Creative Commons BY-SA 3.0 License&lt;/a&gt;. I&#39;ll add a cron job to export the database at the beginning of every month.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=1039</guid>
			
			<pubDate>Wed, 12 Jan 2011 22:38:51 -0000</pubDate>
		</item>
		
		<item>
			<title>MusicBrainz database replication</title>
			<link>http://oxygene.sk/2010/10/musicbrainz-database-replication/</link>
			<comments>http://oxygene.sk/2010/10/musicbrainz-database-replication/#disqus_thread</comments>
			
			<category>musicbrainz</category>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I wanted to work on the Acoustid lookup web service and for this I needed to setup a MusicBrainz replicated database slave. Normally I&#39;d use the Perl code from mb_server, but I didn&#39;t want to mess up the setup I have on the machine hosting &lt;a href=&quot;http://acoustid.org/&quot;&gt;acoustid.org&lt;/a&gt; with CPAN packages. I had a plan to write a non-Perl version of the replicated code earlier, but only yesterday I really needed it. It turned out to be easier than I expected, so I already have a working version that I use to update my local MB database. &lt;a href=&quot;https://github.com/lalinsky/mbslave&quot;&gt;Get it from GitHub&lt;/a&gt;, if you would like to give it a try.&lt;/p&gt;

&lt;p&gt;It has scripts for importing the database dumps as well as applying replication packets, so it can be used creating a MB database from scratch as well as updating an existing one. Instructions for setting up a new database can be found in the &lt;a href=&quot;https://github.com/lalinsky/mbslave/blob/master/README.txt&quot;&gt;README&lt;/a&gt; file.&lt;/p&gt;

&lt;p&gt;There are a few differences between these scripts and &lt;a href=&quot;http://bugs.musicbrainz.org/browser/mb_server/branches/RELEASE_20090524-BRANCH/admin&quot;&gt;mb_server&lt;/a&gt;:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;Depends only on Python and &lt;a href=&quot;http://initd.org/psycopg/&quot;&gt;psycopg2&lt;/a&gt;. Should be easy to set it up on non-Linux platforms, but I&#39;ve not tested that.&lt;/li&gt;
&lt;li&gt;Doesn&#39;t require additional space for extracting the database dump tarballs. It decompresses and imports the files on-the-fly.&lt;/li&gt;
&lt;li&gt;Can be configured to import the MB database into any schema, not just &quot;public&quot;.&lt;/li&gt;
&lt;li&gt;Can be configured to not replicate specific tables (e.g. direct search indexes, PUIDs).&lt;/li&gt;
&lt;li&gt;Regrouped transactions. The MB replication packet consists of multiple transactions (each of which has multiple database operations). The original replication code in mb_server imports the transactions exactly as they were executed in the master database. My code applies the whole packet in a single transaction. This is a small disadvantage, but from an external point of view (external scripts, triggers), there is no practical difference.&lt;/li&gt;
&lt;li&gt;New code, possibly buggy.&lt;/li&gt;
&lt;/ol&gt;


&lt;p&gt;The code currently assumes that you know how to setup the MusicBrainz database, so it doesn&#39;t do many checks. It it turns out it&#39;s useful also to somebody else, I can make it more robust.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=833</guid>
			
			<pubDate>Sat, 30 Oct 2010 13:06:17 -0000</pubDate>
		</item>
		
		<item>
			<title>Oracle...</title>
			<link>http://oxygene.sk/2010/06/oracle/</link>
			<comments>http://oxygene.sk/2010/06/oracle/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;Working with Oracle is always an adventure. The error messages are usually not very helpful, so you have to guess a lot. What I&#39;ve seen today is an extreme though. Oracle allows you to create a table with a column named &quot;TIMESTAMP&quot; if you quote it:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE TABLE &quot;SOME_TABLE&quot; (
    ...
    &quot;TIMESTAMP&quot; TIMESTAMP WITH TIME ZONE
);
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Oracle is rather picky on identifier names, but since it accepted &quot;TIMESTAMP&quot;, I was assuming everything is fine. Later I needed to create a trigger for this table and that&#39;s where the fun starts.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE OR REPLACE TRIGGER &quot;SOME_TABLE_TR&quot; 
BEFORE INSERT ON &quot;SOME_TABLE&quot;
FOR EACH ROW
BEGIN
    ...
END;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This was failing for some reason though. The only thing I got was this &quot;nice&quot; error message, pointing to the table name in the &lt;code&gt;CREATE TRIGGER&lt;/code&gt; statement:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;ORA-06552: PL/SQL: Compilation unit analysis terminated
ORA-06553: PLS-320: the declaration of the type of this expression is incomplete or malformed
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;What type? Do I have a typo somewhere? Did the table somehow get corrupted? You can&#39;t imaging how long did it take for me to figure out that it doesn&#39;t like the column name, which was not mentioned anywhere in the PL/SQL block. I would have no problem if it told me that I can&#39;t use the name. There are too many restrictions on identifiers anyway. What I don&#39;t understand is why does it allow me to create something that&#39;s going to break other core functionality.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=495</guid>
			
			<pubDate>Wed, 16 Jun 2010 16:37:40 -0000</pubDate>
		</item>
		
		<item>
			<title>UUID generator in PL/pgSQL</title>
			<link>http://oxygene.sk/2009/10/uuid-generator-in-plpgsql/</link>
			<comments>http://oxygene.sk/2009/10/uuid-generator-in-plpgsql/#disqus_thread</comments>
			
			<category>programming</category>
			
			<description>&lt;p&gt;While writing an &lt;a href=&quot;http://bugs.musicbrainz.org/browser/mb_server/branches/RELEASE_20090524-BRANCH/admin/sql/updates/20090402-2.sql&quot;&gt;SQL script&lt;/a&gt; to upgrade the MusicBrainz database for the last release, I needed a way to generate new &lt;a href=&quot;http://en.wikipedia.org/wiki/Universally_Unique_Identifier&quot;&gt;UUIDs&lt;/a&gt; from SQL. PostgreSQL has a native UUID data type and a contrib module for generating UUIDs since version 8.3, but this wouldn&#39;t help me, because I needed it to work with at least version 8.1. I had this idea to write PL/pgSQL functions to generate UUIDs, so I skimmer over the &lt;a href=&quot;http://www.ietf.org/rfc/rfc4122.txt&quot;&gt;RFC 4122&lt;/a&gt; that documents them and found out that it isn&#39;t actually that hard.&lt;/p&gt;

&lt;p&gt;MusicBrainz uses random-based UUIDs (version 4) for all it&#39;s new IDs, so the first idea was to implement the same. I know I can&#39;t use this code in the end, because I need a good pseudo-random number generator, but I couldn&#39;t resist to write it anyway. Messing with bits in high-level languages is always fun :) Here is the result (&lt;em&gt;because of the use of the &lt;code&gt;random()&lt;/code&gt; function, don&#39;t use the code for anything serious&lt;/em&gt;):&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE OR REPLACE FUNCTION generate_uuid_v4() RETURNS uuid
    AS $$
DECLARE
    value VARCHAR(36);
BEGIN
    value =          lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad((to_hex((ceil(random() * 255)::int &amp;amp; 15) | 64)), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad((to_hex((ceil(random() * 255)::int &amp;amp; 63) | 128)), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    RETURN value::uuid;
END;
$$ LANGUAGE &#39;plpgsql&#39;;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;It turned out that we need deterministic IDs to be generated from the script, so V4 was out of question. That was good, because we would need a better PNRG for the final version.&lt;/p&gt;

&lt;p&gt;The next idea was to create the URL on which the new rows will be server and generate name-based UUIDs using the URL namespace. The idea is to concatenate a namespace and a name, calculate a cryptographic hash of the result, and use it&#39;s bits to generate the UUID.  There are two options for hashing, either MD5 (version 3) or SHA-1 (version 5). SHA-1 is preferred by the RFC, but PostgreSQL only has a built-in function for MD5, so the decision for us was easy. The code doesn&#39;t depend any random numbers, so it&#39;s good enough to use in production.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE OR REPLACE FUNCTION from_hex(t text) RETURNS integer
    AS $$
DECLARE
    r RECORD;
BEGIN
    FOR r IN EXECUTE &#39;SELECT x&#39;&#39;&#39;||t||&#39;&#39;&#39;::integer AS hex&#39; LOOP
        RETURN r.hex;
    END LOOP;
END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION generate_uuid_v3(namespace varchar, name varchar) RETURNS uuid
    AS $$
DECLARE
    value varchar(36);
    bytes varchar;
BEGIN
    bytes = md5(decode(namespace, &#39;hex&#39;) || decode(name, &#39;escape&#39;));
    value = substr(bytes, 1+0, 8);
    value = value || &#39;-&#39;;
    value = value || substr(bytes, 1+2*4, 4);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex((from_hex(substr(bytes, 1+2*6, 2)) &amp;amp; 15) | 48), 2, &#39;0&#39;);
    value = value || substr(bytes, 1+2*7, 2);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex((from_hex(substr(bytes, 1+2*8, 2)) &amp;amp; 63) | 128), 2, &#39;0&#39;);
    value = value || substr(bytes, 1+2*9, 2);
    value = value || &#39;-&#39;;
    value = value || substr(bytes, 1+2*10, 12);
    return value::uuid;
END;
$$ LANGUAGE &#39;plpgsql&#39; IMMUTABLE STRICT;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This code should be easy enough to modify to generate UUIDv5, if you have a way to calculate SHA-1 hashes. To use the function, you need to pass it a namespace and a name. The namespace itself is a UUID, it can be anything, but there are a few well-known options:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;strong&gt;URL&lt;/strong&gt;&lt;br /&gt;&lt;code&gt;&#39;6ba7b8119dad11d180b400c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;DNS&lt;/strong&gt; (fully-qualified domain name)&lt;br /&gt;&lt;code&gt;&#39;6ba7b8109dad11d180b400c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;ISO OID&lt;/strong&gt;&lt;br /&gt;&lt;code&gt;&#39;6ba7b8129dad11d180b400c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;X.500 DN&lt;/strong&gt; (in DER or a text output format)&lt;br /&gt;&lt;code&gt;&#39;6ba7b814-9dad-11d1-80b4-00c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;The URL one is probably the most useful. So, to generate UUIDv3 for &lt;code&gt;http://www.example.com/foo/1&lt;/code&gt;, you can use the following:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;SELECT generate_uuid_v3(&#39;6ba7b8119dad11d180b400c04fd430c8&#39;, &#39;http://www.example.com/foo/1&#39;);
&lt;/code&gt;&lt;/pre&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=38</guid>
			
			<pubDate>Wed, 21 Oct 2009 05:39:18 -0000</pubDate>
		</item>
		
	</channel>
</rss>

