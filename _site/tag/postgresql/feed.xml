<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Lukáš Lalinský - Tag: postgresql</title>
		<description>Random notes and stuff</description>
		<link>http://oxygene.sk</link>
		<language>en</language>
		
		<item>
			<title>Easier MusicBrainz NGS database setup</title>
			<link>http://oxygene.sk/2011/03/easier-musicbrainz-ngs-database-setup/</link>
			<comments>http://oxygene.sk/2011/03/easier-musicbrainz-ngs-database-setup/#disqus_thread</comments>
			
			<category>musicbrainz</category>
			
			<description>&lt;p&gt;&lt;a href=&quot;/2010/10/musicbrainz-database-replication/&quot;&gt;Some time ago&lt;/a&gt; I wrote a couple of tools that help me set up and update a mirror of the MusicBrainz database on the Acoustid server. It turned out to be work really well. Recently I&#39;ve seen a few people struggling with setting up the NGS database using the original server codebase. The official route assumes you are going to run a MB server instance, which makes things a little bit more complicated than it has to be. You have to install a number Perl modules, you have to compile the MusicBrainz-specific PostgreSQL extensions, even though you most likely don&#39;t need them, you are forced to setup a &lt;code&gt;musicbrainz_db_raw&lt;/code&gt; database that you are definitely not going to use, because there is no data in it, etc.&lt;/p&gt;

&lt;p&gt;If you just want to have a local copy of the MB NGS database, don&#39;t want to mess with CPAN or compiling PostgreSQL extensions, you can try the &lt;a href=&quot;https://github.com/lalinsky/mbslave&quot;&gt;NGS version of mbslave&lt;/a&gt;. You only need Python, psycopg2 and PostgreSQL. The number of steps you have to do manually is probably larger than with the official way, but on any Unix-based OS it should be just copy&amp;paste; from the &lt;a href=&quot;https://github.com/lalinsky/mbslave/blob/master/README.txt&quot;&gt;README&lt;/a&gt; file and it should really just work, with the benefit of being able to customize everything.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=1307</guid>
			
			<pubDate>Tue, 29 Mar 2011 10:03:16 -0000</pubDate>
		</item>
		
		<item>
			<title>Acoustid database dump available</title>
			<link>http://oxygene.sk/2011/01/acoustid-database-dump-available/</link>
			<comments>http://oxygene.sk/2011/01/acoustid-database-dump-available/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;I&#39;ve finally written a script to take a consistent dump of the Acoustid database in the PostgreSQL tab-separated format used by the &lt;a href=&quot;http://www.postgresql.org/docs/8.4/static/sql-copy.html&quot;&gt;COPY&lt;/a&gt; command. I do not have any tools for importing it into PostgreSQL, so it has to be done manually by running SQL commands, but if anybody is interested in playing with the database, you can download it &lt;a href=&quot;http://acoustid.org/data/&quot;&gt;here&lt;/a&gt; (2.7G after compression using bzip2, 6.4G uncompressed). The data is licensed under a &lt;a href=&quot;http://creativecommons.org/licenses/by-sa/3.0/&quot;&gt;Creative Commons BY-SA 3.0 License&lt;/a&gt;. I&#39;ll add a cron job to export the database at the beginning of every month.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=1039</guid>
			
			<pubDate>Wed, 12 Jan 2011 22:38:51 -0000</pubDate>
		</item>
		
		<item>
			<title>MusicBrainz database replication</title>
			<link>http://oxygene.sk/2010/10/musicbrainz-database-replication/</link>
			<comments>http://oxygene.sk/2010/10/musicbrainz-database-replication/#disqus_thread</comments>
			
			<category>musicbrainz</category>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I wanted to work on the Acoustid lookup web service and for this I needed to setup a MusicBrainz replicated database slave. Normally I&#39;d use the Perl code from mb_server, but I didn&#39;t want to mess up the setup I have on the machine hosting &lt;a href=&quot;http://acoustid.org/&quot;&gt;acoustid.org&lt;/a&gt; with CPAN packages. I had a plan to write a non-Perl version of the replicated code earlier, but only yesterday I really needed it. It turned out to be easier than I expected, so I already have a working version that I use to update my local MB database. &lt;a href=&quot;https://github.com/lalinsky/mbslave&quot;&gt;Get it from GitHub&lt;/a&gt;, if you would like to give it a try.&lt;/p&gt;

&lt;p&gt;It has scripts for importing the database dumps as well as applying replication packets, so it can be used creating a MB database from scratch as well as updating an existing one. Instructions for setting up a new database can be found in the &lt;a href=&quot;https://github.com/lalinsky/mbslave/blob/master/README.txt&quot;&gt;README&lt;/a&gt; file.&lt;/p&gt;

&lt;p&gt;There are a few differences between these scripts and &lt;a href=&quot;http://bugs.musicbrainz.org/browser/mb_server/branches/RELEASE_20090524-BRANCH/admin&quot;&gt;mb_server&lt;/a&gt;:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;Depends only on Python and &lt;a href=&quot;http://initd.org/psycopg/&quot;&gt;psycopg2&lt;/a&gt;. Should be easy to set it up on non-Linux platforms, but I&#39;ve not tested that.&lt;/li&gt;
&lt;li&gt;Doesn&#39;t require additional space for extracting the database dump tarballs. It decompresses and imports the files on-the-fly.&lt;/li&gt;
&lt;li&gt;Can be configured to import the MB database into any schema, not just &quot;public&quot;.&lt;/li&gt;
&lt;li&gt;Can be configured to not replicate specific tables (e.g. direct search indexes, PUIDs).&lt;/li&gt;
&lt;li&gt;Regrouped transactions. The MB replication packet consists of multiple transactions (each of which has multiple database operations). The original replication code in mb_server imports the transactions exactly as they were executed in the master database. My code applies the whole packet in a single transaction. This is a small disadvantage, but from an external point of view (external scripts, triggers), there is no practical difference.&lt;/li&gt;
&lt;li&gt;New code, possibly buggy.&lt;/li&gt;
&lt;/ol&gt;


&lt;p&gt;The code currently assumes that you know how to setup the MusicBrainz database, so it doesn&#39;t do many checks. It it turns out it&#39;s useful also to somebody else, I can make it more robust.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=833</guid>
			
			<pubDate>Sat, 30 Oct 2010 13:06:17 -0000</pubDate>
		</item>
		
		<item>
			<title>Acoustid</title>
			<link>http://oxygene.sk/2010/08/acoustid/</link>
			<comments>http://oxygene.sk/2010/08/acoustid/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;After asking for fingerprint test data in my &lt;a href=&quot;/2010/07/introducing-chromaprint/&quot;&gt;last post about Chromaprint&lt;/a&gt; I received about 270k fingerprints from. This helped me to perform some larger tests on the proof-of-concept lookup server I had implemented using Java and PostgreSQL. I had to do some technical changes, but the main idea seemed to work pretty well. I wasn&#39;t sure about this when I was working on Chromaprint, but now I believe that I can realistically run an audio recognition service.&lt;/p&gt;

&lt;p&gt;So, let me introduce &lt;a href=&quot;https://launchpad.net/acoustid-server&quot;&gt;Acoustid&lt;/a&gt; (named by &lt;a href=&quot;http://chatlogs.musicbrainz.org/musicbrainz/2010/2010-07/2010-07-14.html#T11-27-54-618882&quot;&gt;herojoker&lt;/a&gt;), the server side component for audio fingerprint lookups. As I mentioned before, it uses PostgreSQL&#39;s &lt;a href=&quot;http://developer.postgresql.org/pgdocs/postgres/gin.html&quot;&gt;GIN indexes&lt;/a&gt; with the &lt;a href=&quot;http://www.postgresql.org/docs/8.4/static/intarray.html&quot;&gt;intarray&lt;/a&gt; implemented to do the hard work. Java is used as a glue language that provides an web-based service for interacting with the database. I originally considered using C++ for this (using the &lt;a href=&quot;http://pocoproject.org/&quot;&gt;POCO libraries&lt;/a&gt;), but there is significantly more tools in Java for an application like this, so even though I don&#39;t particularly like Java, it seemed like the best language for this.&lt;/p&gt;

&lt;p&gt;Chromaprint fingerprints are basically timed sequences of 32-bit numbers (&quot;subfingerprints&quot;). They are extracted from the audio stream, using a technique which is not important here, every 0.1238 seconds and cover about 1.9814 seconds of the following audio data. This means that to represent a minute of audio, you need around 460 of such numbers. For two audio files representing the same song in different file formats, these numbers should be very similar in terms of bit errors. Acoustid&#39;s job is to take these numbers and find a fingerprint in the database that has the least bit errors compared to the query. Theoretically, this could be used to identify songs based on a few seconds of the original audio, but that would require indexing whole fingerprints, because we don&#39;t which part of the original fingerprint does the query match. This requires a lot of RAM to perform efficiently, so I decided to collect the data that is necessary to perform searches like this, but not implement the feature for now.&lt;/p&gt;

&lt;p&gt;Instead, Acoustid&#39;s primary task is to identify audio files and for this purpose it can assume that the query is extracted from the beginning of the audio. It indexes only ~15 (currently from 0:25 to 0:40) seconds of audio and uses this index to search for possible matches. Currently it assumes that at least one number from the range matches exactly. This can yield some false positive matches, so the next step is to compare the full query to the full fingerprint. As the fingerprints might not be ideally aligned, it first finds the best alignment and then calculates a score as the number of subfingerprints with less than 2 bit errors. This matching and scoring is done completely on the database server, using the intarray extension and a custom C extension for calculating the score.&lt;/p&gt;

&lt;p&gt;This seem to all work fine from technical point of view. What I&#39;m fighting with is the API for this. How to pass the fingerprint data efficiently, how to authenticate users for submissions, whether to have the concept of users at all. The main problems currently are:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;&lt;p&gt;The fingerprints are quite large and it would be ideal to send them as binary data. This conflicts with most existing standards for web services. I&#39;ve implemented a compression algorithm that makes the size of base64-encoded fingerprint similar to the size of an uncompressed binary fingerprint, but I&#39;d still like to not waste the bandwidth if I can avoid it. I&#39;ve considered &lt;a href=&quot;http://en.wikipedia.org/wiki/Bencode&quot;&gt;bencode&lt;/a&gt;-formatted requests, but that is probably too non-standard. I&#39;ve also considered accepting &quot;Content-Encoding: gzip&quot; in HTTP requests, which everybody seems to be using only for responses, but it seems usable also for requests.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;I&#39;d like to track who submitted which fingerprints. I don&#39;t need any private data, but I&#39;d like to be able to delete all fingerprints from one source if I find out that there is something wrong with the data. Maybe I&#39;m paranoid, but I&#39;m afraid that some people might try to submit wrong data in order to reduce the usefulnes of the database.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;If I want to track users, where should the user accounts come from. I definitely don&#39;t want people to have to register and I&#39;d like the service to be integrated with the MusicBrainz database, so accepting MB usernames/passports would be one option. Another option would be to go use any &lt;a href=&quot;http://openid.net/&quot;&gt;OpenID&lt;/a&gt; account and use something like &lt;a href=&quot;http://oauth.net/&quot;&gt;OAuth&lt;/a&gt; for API authentication. I guess some people would prefer to not use their Google/Yahoo/etc. OpenID accounts, so perhaps having MB as an OpenID provider would help?&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;There is also the question of how to authenticate the users. If I accept MB username/passwords I could use standard HTTP Digest authentication, but normally that means that applications would have to send most authenticated HTTP requests twice. Only submissions would have to be authenticated and these will have a lot of data in the HTTP body, it would be a waste to send them twice. Other options are OAuth or a custom token-based method.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;How to handle metadata? Should it rely only on MusicBrainz identifiers? That would make it less usable for taggers that prefer other metadata databases. Should it allow submissions that are not in MusicBrainz?&lt;/p&gt;&lt;/li&gt;
&lt;/ol&gt;


&lt;p&gt;Regarding licensing, the source code is distributed under the GPLv3 license. I&#39;ve considered more permissible licenses, but while there isn&#39;t much code yet, I&#39;d like to be sure that any improvements stay open source. The service is not yet live, so perhaps it&#39;s too soon to speak about data licensing, but I meant to use something like &lt;a href=&quot;http://creativecommons.org/licenses/by-sa/2.0/&quot;&gt;CC BY-SA&lt;/a&gt; for the data (including database dumps), but not allow commercial usage of the free service. If there will be a commercial application interested in the service in the future, it should either run their own mirror or help paying the hosting costs so that the service can stay free for free applications.&lt;/p&gt;

&lt;p&gt;Anyway, the source code is on &lt;a href=&quot;https://code.launchpad.net/acoustid-server&quot;&gt;Launchpad&lt;/a&gt; and there is a mailing list on &lt;a href=&quot;http://groups.google.com/group/acoustid&quot;&gt;Google Groups&lt;/a&gt;. There is also a &lt;a href=&quot;http://acoustid.oxygene.sk/wiki/Main_Page&quot;&gt;wiki&lt;/a&gt;, which will eventually contain documentation for the project. If you would like to help me with finishing this project or are just interested in its future, please join the mailing list.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=625</guid>
			
			<pubDate>Tue, 31 Aug 2010 09:08:50 -0000</pubDate>
		</item>
		
		<item>
			<title>UUID generator in PL/pgSQL</title>
			<link>http://oxygene.sk/2009/10/uuid-generator-in-plpgsql/</link>
			<comments>http://oxygene.sk/2009/10/uuid-generator-in-plpgsql/#disqus_thread</comments>
			
			<category>programming</category>
			
			<description>&lt;p&gt;While writing an &lt;a href=&quot;http://bugs.musicbrainz.org/browser/mb_server/branches/RELEASE_20090524-BRANCH/admin/sql/updates/20090402-2.sql&quot;&gt;SQL script&lt;/a&gt; to upgrade the MusicBrainz database for the last release, I needed a way to generate new &lt;a href=&quot;http://en.wikipedia.org/wiki/Universally_Unique_Identifier&quot;&gt;UUIDs&lt;/a&gt; from SQL. PostgreSQL has a native UUID data type and a contrib module for generating UUIDs since version 8.3, but this wouldn&#39;t help me, because I needed it to work with at least version 8.1. I had this idea to write PL/pgSQL functions to generate UUIDs, so I skimmer over the &lt;a href=&quot;http://www.ietf.org/rfc/rfc4122.txt&quot;&gt;RFC 4122&lt;/a&gt; that documents them and found out that it isn&#39;t actually that hard.&lt;/p&gt;

&lt;p&gt;MusicBrainz uses random-based UUIDs (version 4) for all it&#39;s new IDs, so the first idea was to implement the same. I know I can&#39;t use this code in the end, because I need a good pseudo-random number generator, but I couldn&#39;t resist to write it anyway. Messing with bits in high-level languages is always fun :) Here is the result (&lt;em&gt;because of the use of the &lt;code&gt;random()&lt;/code&gt; function, don&#39;t use the code for anything serious&lt;/em&gt;):&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE OR REPLACE FUNCTION generate_uuid_v4() RETURNS uuid
    AS $$
DECLARE
    value VARCHAR(36);
BEGIN
    value =          lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad((to_hex((ceil(random() * 255)::int &amp;amp; 15) | 64)), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad((to_hex((ceil(random() * 255)::int &amp;amp; 63) | 128)), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    RETURN value::uuid;
END;
$$ LANGUAGE &#39;plpgsql&#39;;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;It turned out that we need deterministic IDs to be generated from the script, so V4 was out of question. That was good, because we would need a better PNRG for the final version.&lt;/p&gt;

&lt;p&gt;The next idea was to create the URL on which the new rows will be server and generate name-based UUIDs using the URL namespace. The idea is to concatenate a namespace and a name, calculate a cryptographic hash of the result, and use it&#39;s bits to generate the UUID.  There are two options for hashing, either MD5 (version 3) or SHA-1 (version 5). SHA-1 is preferred by the RFC, but PostgreSQL only has a built-in function for MD5, so the decision for us was easy. The code doesn&#39;t depend any random numbers, so it&#39;s good enough to use in production.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE OR REPLACE FUNCTION from_hex(t text) RETURNS integer
    AS $$
DECLARE
    r RECORD;
BEGIN
    FOR r IN EXECUTE &#39;SELECT x&#39;&#39;&#39;||t||&#39;&#39;&#39;::integer AS hex&#39; LOOP
        RETURN r.hex;
    END LOOP;
END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION generate_uuid_v3(namespace varchar, name varchar) RETURNS uuid
    AS $$
DECLARE
    value varchar(36);
    bytes varchar;
BEGIN
    bytes = md5(decode(namespace, &#39;hex&#39;) || decode(name, &#39;escape&#39;));
    value = substr(bytes, 1+0, 8);
    value = value || &#39;-&#39;;
    value = value || substr(bytes, 1+2*4, 4);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex((from_hex(substr(bytes, 1+2*6, 2)) &amp;amp; 15) | 48), 2, &#39;0&#39;);
    value = value || substr(bytes, 1+2*7, 2);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex((from_hex(substr(bytes, 1+2*8, 2)) &amp;amp; 63) | 128), 2, &#39;0&#39;);
    value = value || substr(bytes, 1+2*9, 2);
    value = value || &#39;-&#39;;
    value = value || substr(bytes, 1+2*10, 12);
    return value::uuid;
END;
$$ LANGUAGE &#39;plpgsql&#39; IMMUTABLE STRICT;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This code should be easy enough to modify to generate UUIDv5, if you have a way to calculate SHA-1 hashes. To use the function, you need to pass it a namespace and a name. The namespace itself is a UUID, it can be anything, but there are a few well-known options:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;strong&gt;URL&lt;/strong&gt;&lt;br /&gt;&lt;code&gt;&#39;6ba7b8119dad11d180b400c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;DNS&lt;/strong&gt; (fully-qualified domain name)&lt;br /&gt;&lt;code&gt;&#39;6ba7b8109dad11d180b400c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;ISO OID&lt;/strong&gt;&lt;br /&gt;&lt;code&gt;&#39;6ba7b8129dad11d180b400c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;X.500 DN&lt;/strong&gt; (in DER or a text output format)&lt;br /&gt;&lt;code&gt;&#39;6ba7b814-9dad-11d1-80b4-00c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;The URL one is probably the most useful. So, to generate UUIDv3 for &lt;code&gt;http://www.example.com/foo/1&lt;/code&gt;, you can use the following:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;SELECT generate_uuid_v3(&#39;6ba7b8119dad11d180b400c04fd430c8&#39;, &#39;http://www.example.com/foo/1&#39;);
&lt;/code&gt;&lt;/pre&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=38</guid>
			
			<pubDate>Wed, 21 Oct 2009 05:39:18 -0000</pubDate>
		</item>
		
	</channel>
</rss>

