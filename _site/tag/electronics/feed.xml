<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Lukáš Lalinský - Tag: electronics</title>
		<description>Random notes and stuff</description>
		<link>http://oxygene.sk</link>
		<language>en</language>
		
		<item>
			<title>Using Raspberry Pi as an Arduino (AVR) programmer</title>
			<link>http://oxygene.sk/2015/02/using-raspberry-pi-as-an-arduino-avr-programmer/</link>
			<comments>http://oxygene.sk/2015/02/using-raspberry-pi-as-an-arduino-avr-programmer/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I bought a cheap Arduino nano clone from eBay and after I plugging it in via
USB, I was getting only errors when trying to upload some code. I realized
it came without the bootloader installed. And I didn&#39;t have a normal AVR ISP
programmer to program the flash memory on the microcontroller.&lt;/p&gt;

&lt;p&gt;The ATmega328P chip can be programmed using the SPI protocol. The process
requires 4 digital wires and Raspberry Pi with its GPIO pins
can be easily used to do that. On Linux, you can interact with GPIO pins
using a sysfs interface and avrdude (the application that can program the chip)
can talk the SPI protocol over this interface. So it&#39;s just a
matter of connecting the pieces together. Normally you would use
the AVR ISP pin header on Arduino to do this, but I did not have the
right connector for that, so I used regular pins and connect them on a
breadboard.&lt;/p&gt;

&lt;p&gt;You need to use specific pins on the Arduino, but you can use
any of the available pins on the Raspberry Pi. I used these:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Pin D11 (MOSI) on Arduino to pin 20 on RPi&lt;/li&gt;
&lt;li&gt;Pin D12 (MISO) on Arduino to pin 16 on RPi&lt;/li&gt;
&lt;li&gt;Pin D13 (SCK) on Arduino to pin 21 on RPi&lt;/li&gt;
&lt;li&gt;Pin RESET on Arduino to pin 12 on RPi&lt;/li&gt;
&lt;li&gt;GND on Arduino to GND on RPi&lt;/li&gt;
&lt;li&gt;5V on Arduino to 3.3V on RPi&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Arduino should normally be powered by 5V, but Raspberry Pi
needs 3.3V on GPIO pins, so we will run the Arduino at
lower voltage. It should be fine for the programming.&lt;/p&gt;

&lt;p&gt;The result looked like this:&lt;/p&gt;

&lt;p&gt;&lt;a href=&quot;/uploads/rpi-arduino/IMG_20150202_145746.jpg&quot;&gt;
  &lt;img src=&quot;/uploads/rpi-arduino/IMG_20150202_145746-small.jpg&quot; /&gt;
&lt;/a&gt;
&lt;a href=&quot;/uploads/rpi-arduino/IMG_20150202_145722.jpg&quot;&gt;
  &lt;img src=&quot;/uploads/rpi-arduino/IMG_20150202_145722-small.jpg&quot; /&gt;
&lt;/a&gt;
&lt;a href=&quot;/uploads/rpi-arduino/IMG_20150202_145732.jpg&quot;&gt;
  &lt;img src=&quot;/uploads/rpi-arduino/IMG_20150202_145732-small.jpg&quot; /&gt;
&lt;/a&gt;&lt;/p&gt;

&lt;p&gt;Now the software part. The avrdude package that comes with Raspbian is not
compiled with linuxgpio support, so we need to build our own.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo apt-get install bison flex libusb-dev
cd /tmp
wget http://download.savannah.gnu.org/releases/avrdude/avrdude-6.1.tar.gz
tar xf avrdude-6.1.tar.gz
cd avrdude-6.1
./configure --prefix=/opt/avrdude --enable-linuxgpio
make
sudo make install
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;You need to edit &lt;code&gt;avrdude.conf&lt;/code&gt; to let it know the pins used.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo vim /opt/avrdude/etc/avrdude.conf
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Somewhere in the file find the commented out &lt;code&gt;linuxgpio&lt;/code&gt; section and add this in there.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;programmer
  id    = &quot;linuxgpio&quot;;
  desc  = &quot;Use the Linux sysfs interface to bitbang GPIO lines&quot;;
  type  = &quot;linuxgpio&quot;;
  reset = 12;
  sck   = 21;
  mosi  = 20;
  miso  = 16;
;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;If everything is connected and configured properly, you should be able to run this now and get into the avrdude console.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo /opt/avrdude/bin/avrdude -p atmega328p -c linuxgpio -v -t
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Now we need to modify the Arduino IDE configuration to recognize the new programmer. I don&#39;t know if this can be done locally, so we will edit the system-wide configuration file.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo vim /usr/share/arduino/hardware/arduino/programmers.txt
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Put this somewhere near the end of the file.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;rpigpio.name=RPi GPIO
rpigpio.protocol=linuxgpio
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;We also need to tell the Arduino IDE to use our version of avrdude. Again, because I don&#39;t know of any better solution, we will overwrite the default symlinks that come with the package.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo ln -fs /opt/avrdude/bin/avrdude /usr/share/arduino/hardware/tools/avrdude
sudo ln -fs /opt/avrdude/etc/avrdude.conf /usr/share/arduino/hardware/tools/avrdude.conf
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;That was the last configuration step and now we can actually program the chip.
Becuse you can&#39;t use the Linux GPIO sysft interface as a regular user, you need to start the Arduino IDE as root for this.
Select your board type in &quot;Tools -&gt; Board&quot;, select the &quot;RPi GPIO&quot; option in &quot;Tools -&gt; Programmer&quot; and then install the bootloader using &quot;Tools -&gt; Burn Bootloader&quot;.
Once the bootloader is installed you can close the IDE, remove the pin connections, plug in USB cable and start the IDE as a regular user.&lt;/p&gt;

&lt;p&gt;Hope that helps somebody.&lt;/p&gt;
</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2015/02/using-raspberry-pi-as-an-arduino-avr-programmer/</guid>
			
			<pubDate>Mon, 02 Feb 2015 00:00:00 -0000</pubDate>
		</item>
		
	</channel>
</rss>

