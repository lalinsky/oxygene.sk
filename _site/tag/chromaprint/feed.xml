<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Lukáš Lalinský - Tag: chromaprint</title>
		<description>Random notes and stuff</description>
		<link>http://oxygene.sk</link>
		<language>en</language>
		
		<item>
			<title>Chromaprint 1.4 released</title>
			<link>http://oxygene.sk/2016/12/chromaprint-1-4-released/</link>
			<comments>http://oxygene.sk/2016/12/chromaprint-1-4-released/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;A new version of Chromaprint has been released. This is a fairly big release I originally intended to call 2.0,
but one key feature I was planning to include is not yet finished, so I decided to go with 1.4 instead.&lt;/p&gt;

&lt;p&gt;So what&#39;s new?&lt;/p&gt;

&lt;p&gt;The biggest feature is that all components of audio fingerprinting process now work in a streaming fashion and
can provide partial results at any time. That means that it&#39;s now possible to feed a continuous audio stream
to the process and get back partial fingerprints. This is useful if you want to fingerprint e.g. an internet radio stream
and do not want to explicitly split the stream into small chunks. There are also side effects of these changes and
the whole process is now faster and uses less memory.&lt;/p&gt;

&lt;p&gt;This change is also reflected in the fpcalc utility, which can now work on streams.
In fact, the fpcalc utility has been completely rewritten. It now also supports JSON output for easier integration,
since you can find JSON parser in the standard library of almost every new programing language.&lt;/p&gt;

&lt;p&gt;Here is an example using an online radio:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;$ fpcalc -ts -chunk 10 -overlap -json http://icecast2.play.cz/radio1.mp3
{&quot;timestamp&quot;: 1480789416.14, &quot;duration&quot;: 12.60, &quot;fingerprint&quot;: &quot;AQAAUFSSTEnCRBKeZsR0...&quot;}
{&quot;timestamp&quot;: 1480789423.68, &quot;duration&quot;: 12.60, &quot;fingerprint&quot;: &quot;AQAAUZOSRIsUTkGz9SCZ...&quot;}
{&quot;timestamp&quot;: 1480789433.68, &quot;duration&quot;: 12.60, &quot;fingerprint&quot;: &quot;AQAAUYmWfJIoiNIRHzsl...&quot;}
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Or you can fingerprint raw pcm data from an external audio input:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;$ arecord --format=S16 --rate=44100 --channels=1 --file-type=raw \
    | fpcalc -format s16le -rate 44100 -channels 1 -ts -chunk 10 -overlap -json -
{&quot;timestamp&quot;: 1480789965.67, &quot;duration&quot;: 12.60, &quot;fingerprint&quot;: &quot;AQAAUFqkMVmyJMHWBa97...&quot;}
{&quot;timestamp&quot;: 1480789975.81, &quot;duration&quot;: 12.60, &quot;fingerprint&quot;: &quot;AQAAUVGYSEkXJvit4ce1...&quot;}
{&quot;timestamp&quot;: 1480789985.72, &quot;duration&quot;: 12.60, &quot;fingerprint&quot;: &quot;AQAAUcoSpRunwN6Op_hy...&quot;}
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This opens up many options how Chromaprint can be used outside of AcoustID.&lt;/p&gt;

&lt;p&gt;There has also been a big source code cleanup. A lot of old code has been removed. We started using C++11 features,
so it&#39;s no longer possible to compile Chromaprintwith old C++ compilers. Unit tests no longer depend on boost.
KissFFT is now bundled in the package and used as a fallback if no other FFT library is found. That means Chromaprint can be now
build without any external dependencies.&lt;/p&gt;

&lt;p&gt;The public C API now uses standard fixed-size int types from stdint.h. This breaks API-level backwards compatibility,
you will need to modify your programs. The binary interface has not changed and programs compiled against the old version of
the library will continue working.&lt;/p&gt;

&lt;p&gt;All source code from Chromaprint written by me has been relicensed from LGPL to MIT. Note that this does not mean much for the
library as a whole, since it still depends on LGPL code, but it&#39;s now easier to reuse parts of the code in other projects, if needed.&lt;/p&gt;

&lt;p&gt;We also have fpcalc binaries for ARM processors now. They were built and tested on Raspberry Pi, but might work on other ARM devices.&lt;/p&gt;

&lt;p&gt;Download:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-1.4.tar.gz&quot;&gt;Source code tarball&lt;/a&gt; (597 KB)&lt;/li&gt;
&lt;li&gt;Static binaries for the &lt;code&gt;fpcalc&lt;/code&gt; tool

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.4-windows-i686.zip&quot;&gt;Windows, i686&lt;/a&gt; (1.4 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.4-windows-x86_64.zip&quot;&gt;Windows, x86_64&lt;/a&gt; (1.5 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.4-macos-i386.tar.gz&quot;&gt;macOS, i386&lt;/a&gt; (1.1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.4-macos-x86_64.tar.gz&quot;&gt;macOS, x86_64&lt;/a&gt; (1.2 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.4-linux-i686.tar.gz&quot;&gt;Linux, i686&lt;/a&gt; (1.3 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.4-linux-x86_64.tar.gz&quot;&gt;Linux, x86_64&lt;/a&gt; (1.2 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.4-linux-armhf.tar.gz&quot;&gt;Linux, armhf&lt;/a&gt; (1.2 MB)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2016/12/chromaprint-1-4-released/</guid>
			
			<pubDate>Sat, 03 Dec 2016 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Chromaprint 1.3.2 released</title>
			<link>http://oxygene.sk/2016/07/chromaprint-1-3-2-released/</link>
			<comments>http://oxygene.sk/2016/07/chromaprint-1-3-2-released/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;A new version of Chromaprint has been released.
This is a very small bug fix release fixing fpcalc crash on a corrupt file.&lt;/p&gt;

&lt;p&gt;Changes since version 1.3.1:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Fixed crash on an invalid audio file that FFmpeg could not decode.&lt;/li&gt;
&lt;li&gt;Fixed build on Ubuntu 14.04 with libav.&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Download:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-1.3.2.tar.gz&quot;&gt;Source code tarball&lt;/a&gt; (525 KB)&lt;/li&gt;
&lt;li&gt;Static binaries for the &lt;code&gt;fpcalc&lt;/code&gt; tool

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.2-win-i686.zip&quot;&gt;Windows, 32-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.2-win-x86_64.zip&quot;&gt;Windows, 64-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.2-osx-i386.tar.gz&quot;&gt;Mac OS X, 32-bit, 10.4+&lt;/a&gt; (964 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.2-osx-x86_64.tar.gz&quot;&gt;Mac OS X, 64-bit, 10.4+&lt;/a&gt; (944 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.2-linux-i686.tar.gz&quot;&gt;Linux, 32-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.2-linux-x86_64.tar.gz&quot;&gt;Linux, 64-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2016/07/chromaprint-1-3-2-released/</guid>
			
			<pubDate>Sat, 09 Jul 2016 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Chromaprint 1.3.1 released</title>
			<link>http://oxygene.sk/2016/02/chromaprint-1-3-1-released/</link>
			<comments>http://oxygene.sk/2016/02/chromaprint-1-3-1-released/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;A new version of Chromaprint has been released.&lt;/p&gt;

&lt;p&gt;Changes since version 1.3:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Fixed &lt;code&gt;fpcalc -length&lt;/code&gt; to actually restrict fingerprints the requested length.&lt;/li&gt;
&lt;li&gt;Fixed SONAME version for the shared library.&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Download:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-1.3.1.tar.gz&quot;&gt;Source code tarball&lt;/a&gt; (525 KB)&lt;/li&gt;
&lt;li&gt;Static binaries for the &lt;code&gt;fpcalc&lt;/code&gt; tool

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.1-win-i686.zip&quot;&gt;Windows, 32-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.1-win-x86_64.zip&quot;&gt;Windows, 64-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.1-osx-i386.tar.gz&quot;&gt;Mac OS X, 32-bit, 10.4+&lt;/a&gt; (964 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.1-osx-x86_64.tar.gz&quot;&gt;Mac OS X, 64-bit, 10.4+&lt;/a&gt; (944 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.1-linux-i686.tar.gz&quot;&gt;Linux, 32-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3.1-linux-x86_64.tar.gz&quot;&gt;Linux, 64-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2016/02/chromaprint-1-3-1-released/</guid>
			
			<pubDate>Wed, 10 Feb 2016 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Chromaprint 1.3 released</title>
			<link>http://oxygene.sk/2016/02/chromaprint-1-3-released/</link>
			<comments>http://oxygene.sk/2016/02/chromaprint-1-3-released/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;A new version of Chromaprint has been released. This is another small release, there are no changes to the core functionality.&lt;/p&gt;

&lt;p&gt;Changes since version 1.2:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;The binary packages have been built with FFmpeg 2.8.6, adding support for DSF files&lt;/li&gt;
&lt;li&gt;You can use use &lt;code&gt;fpcalc -length 0&lt;/code&gt; to get the full fingerprint&lt;/li&gt;
&lt;li&gt;New function &lt;code&gt;chromaprint_get_fingerprint_hash&lt;/code&gt; for calculating SimHash from the fingerprint data&lt;/li&gt;
&lt;li&gt;Added info section to the fpcalc executable on Mac OS X&lt;/li&gt;
&lt;li&gt;Generate .pc (pkg-config) file on Mac OS X when not building a framework&lt;/li&gt;
&lt;li&gt;Removed use of some long deprecated FFmpeg APIs&lt;/li&gt;
&lt;li&gt;Some smaller bug fixes&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Download:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-1.3.tar.gz&quot;&gt;Source code tarball&lt;/a&gt; (525 KB)&lt;/li&gt;
&lt;li&gt;Static binaries for the &lt;code&gt;fpcalc&lt;/code&gt; tool

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3-win-i686.zip&quot;&gt;Windows, 32-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3-win-x86_64.zip&quot;&gt;Windows, 64-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3-osx-i386.tar.gz&quot;&gt;Mac OS X, 32-bit, 10.4+&lt;/a&gt; (964 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3-osx-x86_64.tar.gz&quot;&gt;Mac OS X, 64-bit, 10.4+&lt;/a&gt; (944 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3-linux-i686.tar.gz&quot;&gt;Linux, 32-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.3-linux-x86_64.tar.gz&quot;&gt;Linux, 64-bit&lt;/a&gt; (1 MB)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;

</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2016/02/chromaprint-1-3-released/</guid>
			
			<pubDate>Wed, 03 Feb 2016 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Five years of AcoustID</title>
			<link>http://oxygene.sk/2015/10/five-years-of-acoustid/</link>
			<comments>http://oxygene.sk/2015/10/five-years-of-acoustid/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;It&#39;s hard to tell the exact date when the &lt;a href=&quot;https://acoustid.org/&quot;&gt;AcoustID&lt;/a&gt; project started, but if we go by the
first entry in the database, it was October 8, 2010. That means project turned five this week!
I thought it&#39;s a good opportunity to gather some statistics from those five years.&lt;/p&gt;

&lt;p&gt;Back in 2010, we were starting from scratch. We had an empty database, while the solution that AcoustID was
replacing (&lt;a href=&quot;https://wiki.musicbrainz.org/Fingerprinting#PUID&quot;&gt;MusicDNS/PUID&lt;/a&gt;) had fingerprints for 4.4 million
MusicBrainz recordings (34% of all MusicBrainz recordings at that time).
It took about two years to catch up with that number.
Today, AcoustID can identify 8.3 million MusicBrainz recordings, which is 54% of all recordings in the MusicBrainz
database. So about twice the size and the fingerprint database is growing faster than MusicBrainz itself, which means
eventually it might be able to identify the most of MusicBrainz recordings.&lt;/p&gt;

&lt;p&gt;Since early 2011, we also started accepting fingerprints without links to the MusicBrainz database and
the number of those has grown even faster, so only a small part of the AcoustID fingerprint database
is actually linked to MusicBrainz now. The total number of unique fingerprints (&quot;AcoustIDs&quot;) in the database is currently 25.5 million.&lt;/p&gt;

&lt;p&gt;Here you can see the numbers on a timeline:&lt;/p&gt;

&lt;iframe width=&quot;600&quot; height=&quot;371&quot; seamless frameborder=&quot;0&quot; scrolling=&quot;no&quot; src=&quot;https://docs.google.com/spreadsheets/d/1EsL3v8k5F4DO8yhiiC-uWFTRF_xqTLU5lSbhY7HSI28/pubchart?oid=532574002&amp;amp;format=interactive&quot;&gt;&lt;/iframe&gt;


&lt;p&gt;Traffic has naturally grown during the five years as well, but similarly to the database size, the growth is mostly linear.
This because of the focus on full audio file tagging and integration with MusicBrainz, which means AcoustID only ends up being used in specialized applications.&lt;/p&gt;

&lt;iframe width=&quot;600&quot; height=&quot;371&quot; seamless frameborder=&quot;0&quot; scrolling=&quot;no&quot; src=&quot;https://docs.google.com/spreadsheets/d/1EsL3v8k5F4DO8yhiiC-uWFTRF_xqTLU5lSbhY7HSI28/pubchart?oid=1788537123&amp;amp;format=interactive&quot;&gt;&lt;/iframe&gt;


&lt;p&gt;&lt;em&gt;Unfortunatelly, the first version released 2010 was pretty minimalistic and did not include request statistics, so we only have these numbers starting from August, 2011.&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;&lt;a href=&quot;https://picard.musicbrainz.org/&quot;&gt;MusicBrainz Picard&lt;/a&gt; is the biggest source of users, which is not surprising, because AcoustID has been created for MusicBrainz Picard.
But there are other free applications that use AcoustID -- &lt;a href=&quot;http://beets.radbox.org/&quot;&gt;beets&lt;/a&gt;, &lt;a href=&quot;http://getmusicbee.com/&quot;&gt;MusicBee&lt;/a&gt;, &lt;a href=&quot;http://www.filebot.net/&quot;&gt;FileBot&lt;/a&gt;,
&lt;a href=&quot;http://www.videolan.org/&quot;&gt;VLC&lt;/a&gt;, &lt;a href=&quot;https://www.clementine-player.org/&quot;&gt;Clementine&lt;/a&gt;, &lt;a href=&quot;http://puddletag.sourceforge.net/&quot;&gt;puddletag&lt;/a&gt;
&lt;a href=&quot;http://kid3.sourceforge.net/&quot;&gt;Kid3&lt;/a&gt;, &lt;a href=&quot;https://quodlibet.readthedocs.org/en/latest/&quot;&gt;Quod Libet&lt;/a&gt; and many many other smaller applications.
There are also a few commercial applications that use AcoustID. The number of applications using the service every month is now above 100 and still growing.&lt;/p&gt;

&lt;iframe width=&quot;600&quot; height=&quot;371&quot; seamless frameborder=&quot;0&quot; scrolling=&quot;no&quot; src=&quot;https://docs.google.com/spreadsheets/d/1EsL3v8k5F4DO8yhiiC-uWFTRF_xqTLU5lSbhY7HSI28/pubchart?oid=210337696&amp;amp;format=interactive&quot;&gt;&lt;/iframe&gt;


&lt;p&gt;It&#39;s quite easy to use AcoustID from about any programming language now.
Chromaprint fingerprints can be generated from &lt;a href=&quot;https://github.com/sampsyo/pyacoustid&quot;&gt;Python&lt;/a&gt;, &lt;a href=&quot;https://github.com/TMXCredit/chromaprint&quot;&gt;Ruby&lt;/a&gt;, &lt;a href=&quot;https://github.com/jameshurst/rust-chromaprint&quot;&gt;Rust&lt;/a&gt;, &lt;a href=&quot;https://github.com/go-fingerprint/gochroma&quot;&gt;Go&lt;/a&gt;, &lt;a href=&quot;https://github.com/parshap/node-fpcalc&quot;&gt;JavaScript&lt;/a&gt; and I&#39;m probably missing a few.
There are wrappers for C# and Java, but those are always developed directly inside the apps that use them.
There is direct support for generating Chromaprint fingerprints in &lt;a href=&quot;http://cgit.freedesktop.org/gstreamer/gst-plugins-bad/tree/ext/chromaprint/gstchromaprint.c&quot;&gt;GStreamer&lt;/a&gt; and recently also &lt;a href=&quot;https://www.ffmpeg.org/ffmpeg-formats.html#chromaprint-1&quot;&gt;FFmpeg&lt;/a&gt;.
And there are also alternative implementations of the Chromaprint algorithm in C# (&lt;a href=&quot;https://github.com/wo80/AcoustID.NET&quot;&gt;1&lt;/a&gt;, &lt;a href=&quot;https://github.com/zsoltsajben/NChromaprint&quot;&gt;2&lt;/a&gt;) and &lt;a href=&quot;https://github.com/bjjb/chromaprint.js&quot;&gt;JavaScript&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;I have not been working on AcoustID very actively lately and I know that there are some things that need to be done,
but I&#39;m still happy that the project is able to run pretty much on its own with very little support, that the architecture
designed five years ago is still capable of handling today&#39;s traffic and I&#39;m not worried that it won&#39;t be able to
handle the traffic five years from now.&lt;/p&gt;

&lt;p&gt;Happy birthday, AcoustID!&lt;/p&gt;
</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2015/10/five-years-of-acoustid/</guid>
			
			<pubDate>Thu, 08 Oct 2015 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Chromaprint 1.2 released</title>
			<link>http://oxygene.sk/2014/08/chromaprint-1-2-released/</link>
			<comments>http://oxygene.sk/2014/08/chromaprint-1-2-released/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;A new version of Chromaprint has been released. This release doesn&#39;t add
any new features to the library, but there are some source code changes
for better compatibility when building the library in non-standard
situations.&lt;/p&gt;

&lt;p&gt;Because the functionality hasn&#39;t changed, this a source code only release.
You can use fpcalc binaries from version 1.1.&lt;/p&gt;

&lt;p&gt;Download:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-1.2.tar.gz&quot;&gt;Source code tarball&lt;/a&gt; (517.2 KB)&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Changes since version 1.1:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Fixed compilation with neither libswresample nor libavresample (#11)&lt;/li&gt;
&lt;li&gt;Fixed compilation with static libav (#10)&lt;/li&gt;
&lt;li&gt;Functions chromaprint_encode_fingerprint and chromaprint_decode_fingerprint
 are changed to accept const pointer as input&lt;/li&gt;
&lt;li&gt;Added support for using the Kiss FFT library (should make Android port easier)&lt;/li&gt;
&lt;li&gt;Removed obsolete dev tools from the package&lt;/li&gt;
&lt;li&gt;More compatible DEBUG() macro&lt;/li&gt;
&lt;/ul&gt;

</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2014/08/chromaprint-1-2-released/</guid>
			
			<pubDate>Tue, 26 Aug 2014 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Chromaprint 1.1 released</title>
			<link>http://oxygene.sk/2013/11/chromaprint-1-1-released/</link>
			<comments>http://oxygene.sk/2013/11/chromaprint-1-1-released/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;A new version of Chromaprint has been released. There are a few bug fixes, fixed compilation error on OS X 10.9 and added support for compilation with libav (FFmpeg is still the preferred library to use with Chromaprint).&lt;/p&gt;

&lt;p&gt;The official binaries have been build with FFmpeg 2.1. I have switched to a more automated build system for the binaries, so there might be new problems with them. If you find they do not work as expected, please let me know.&lt;/p&gt;

&lt;p&gt;Download:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-1.1.tar.gz&quot;&gt;Source code tarball&lt;/a&gt; (529.6 KB)&lt;/li&gt;
&lt;li&gt;Static binaries for the &lt;code&gt;fpcalc&lt;/code&gt; tool

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.1-win-i686.zip&quot;&gt;Windows, 32-bit&lt;/a&gt; (972.3 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.1-win-x86_64.zip&quot;&gt;Windows, 64-bit&lt;/a&gt; (996.4 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.1-osx-i386.tar.gz&quot;&gt;Mac OS X, 32-bit, 10.4+&lt;/a&gt; (901.2 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.1-osx-x86_64.tar.gz&quot;&gt;Mac OS X, 64-bit, 10.4+&lt;/a&gt; (891.1 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.1-linux-i686.tar.gz&quot;&gt;Linux, 32-bit&lt;/a&gt; (1011.4 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.1-linux-x86_64.tar.gz&quot;&gt;Linux, 64-bit&lt;/a&gt; (998.9 KB)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Changes since version 1.0:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Fixed potential DoS attack in fingerprint decompression code. (#6)&lt;/li&gt;
&lt;li&gt;Fixed invalid memory read with some fingerprinter configurations. (#5)&lt;/li&gt;
&lt;li&gt;Fixed compilation with clang on OS X 10.9 (#7, #9)&lt;/li&gt;
&lt;li&gt;Added support for audio format conversion with libav. (#2, Gordon Pettey)&lt;/li&gt;
&lt;/ul&gt;

</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2013/11/chromaprint-1-1-released/</guid>
			
			<pubDate>Sat, 23 Nov 2013 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>New binaries for Chromaprint 1.0</title>
			<link>http://oxygene.sk/2013/09/new-binaries-for-chromaprint-1-0/</link>
			<comments>http://oxygene.sk/2013/09/new-binaries-for-chromaprint-1-0/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;The &lt;code&gt;fpcalc&lt;/code&gt; binaries included in the &lt;a href=&quot;http://oxygene.sk/2013/09/chromaprint-1-0-released/&quot;&gt;Chromaprint 1.0&lt;/a&gt;
release had a number of problems. They didn&#39;t work on 64-bit Windows and they did not include support for
reading FLAC files.&lt;/p&gt;

&lt;p&gt;I&#39;ve created new builds, which add back FLAC support and the Windows binaries are now created with &lt;a href=&quot;http://mingw-w64.sourceforge.net/&quot;&gt;mingw-w64&lt;/a&gt;,
so we have both 32-bit and 64-bit versions (the 32-bit one should work on 64-bit Windows as well).&lt;/p&gt;

&lt;p&gt;Download:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-1-win-i686.zip&quot;&gt;Windows, 32-bit&lt;/a&gt; (914.7 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-1-win-x86_64.zip&quot;&gt;Windows, 64-bit&lt;/a&gt; (935.6 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-1-osx-i386.tar.gz&quot;&gt;Mac OS X, 32-bit, 10.4+&lt;/a&gt; (878.9 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-1-osx-x86_64.tar.gz&quot;&gt;Mac OS X, 64-bit, 10.4+&lt;/a&gt; (870.7 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-1-linux-i686.tar.gz&quot;&gt;Linux, 32-bit&lt;/a&gt; (985.8 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-1-linux-x86_64.tar.gz&quot;&gt;Linux, 64-bit&lt;/a&gt; (995.1 KB)&lt;/li&gt;
&lt;/ul&gt;

</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2013/09/new-binaries-for-chromaprint-1-0/</guid>
			
			<pubDate>Fri, 27 Sep 2013 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Chromaprint 1.0 released</title>
			<link>http://oxygene.sk/2013/09/chromaprint-1-0-released/</link>
			<comments>http://oxygene.sk/2013/09/chromaprint-1-0-released/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;A new version of Chromaprint has been released. There are no changes to the library, just the &lt;code&gt;fpcalc&lt;/code&gt; utility. The main changes are support for the latest version of the FFmpeg.&lt;/p&gt;

&lt;p&gt;The official binaries have been rebuilt with FFmpeg 2.0.1, with added support for reading audio from more video formats. If they are not able to read audio from some format that you use, please let me know.&lt;/p&gt;

&lt;p&gt;Download:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-1.0.tar.gz&quot;&gt;Source code tarball&lt;/a&gt; (528.7 KB)&lt;/li&gt;
&lt;li&gt;Static binaries for the &lt;code&gt;fpcalc&lt;/code&gt; tool

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-win32.zip&quot;&gt;Windows&lt;/a&gt; (1005.6 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-osx-i386.tar.gz&quot;&gt;Mac OS X, 32-bit, 10.4+&lt;/a&gt; (963.2 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-osx-x86_64.tar.gz&quot;&gt;Mac OS X, 64-bit, 10.4+&lt;/a&gt; (954.7 KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-linux-i686.tar.gz&quot;&gt;Linux, 32-bit&lt;/a&gt; (1.1 MB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://bitbucket.org/acoustid/chromaprint/downloads/chromaprint-fpcalc-1.0-linux-x86_64.tar.gz&quot;&gt;Linux, 64-bit&lt;/a&gt; (1.1 MB)&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Changes since version 1.0:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Support for the latest FFmpeg API.&lt;/li&gt;
&lt;li&gt;Support for reading audio from stdin in fpcalc.&lt;/li&gt;
&lt;li&gt;Changed fpcalc to return non-zero status on failure.&lt;/li&gt;
&lt;/ul&gt;

</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2013/09/chromaprint-1-0-released/</guid>
			
			<pubDate>Sun, 08 Sep 2013 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Chromaprint 0.7 released</title>
			<link>http://oxygene.sk/2012/09/chromaprint-0-7-released/</link>
			<comments>http://oxygene.sk/2012/09/chromaprint-0-7-released/#disqus_thread</comments>
			
			<category>acoustid</category>
			
			<description>&lt;p&gt;A new version of Chromaprint has been released. The main change is compatibility with the latest FFmpeg API. There is also a modified fingerprint algorithm, that ignores leading silence up to a specific threshold, but this has no impact on Acoustid users.&lt;/p&gt;

&lt;p&gt;Functionality of the &lt;code&gt;fpcalc&lt;/code&gt; utility has not changed, so I&#39;m not building the binaries for this release. You can use the binaries from version 0.6.&lt;/p&gt;

&lt;p&gt;Download:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://github.com/downloads/lalinsky/chromaprint/chromaprint-0.7.tar.gz&quot;&gt;Source code tarball&lt;/a&gt; (530K)&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Changes since version 0.6:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Support for the latest FFmpeg API.&lt;/li&gt;
&lt;li&gt;New (non-default) fingerprint algorithm that removes leading silence.&lt;/li&gt;
&lt;li&gt;API to configure fingerprint algorithms, currently only used by &lt;code&gt;CHROMAPRINT_ALGORITHM_TEST4&lt;/code&gt;.&lt;/li&gt;
&lt;/ul&gt;

</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2012/09/chromaprint-0-7-released/</guid>
			
			<pubDate>Wed, 05 Sep 2012 00:00:00 -0000</pubDate>
		</item>
		
	</channel>
</rss>

