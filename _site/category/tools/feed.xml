<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Lukáš Lalinský - Category: Tools</title>
		<description>Random notes and stuff</description>
		<link>http://oxygene.sk</link>
		<language>en</language>
		
		<item>
			<title>Let's Encrypt and Nginx</title>
			<link>http://oxygene.sk/2016/01/lets-encrypt-and-nginx/</link>
			<comments>http://oxygene.sk/2016/01/lets-encrypt-and-nginx/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I&#39;m late to the game, but I finally gave &lt;a href=&quot;https://letsencrypt.org/&quot;&gt;Let&#39;s Encrypt&lt;/a&gt; a try
and I love it. The biggest advantage is the fact that SSL certificates
can be completely automated. No more remembering how to renew certificates
once a year.&lt;/p&gt;

&lt;p&gt;These are mostly just notes for my future use, but maybe it will be useful for somebody.
This is how I use Let&#39;s Encrypt with Nginx.&lt;/p&gt;

&lt;p&gt;Install the letsencrypt client:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;cd /opt
git clone https://github.com/letsencrypt/letsencrypt
VENV_PATH=/opt/letsencrypt/env/ /opt/letsencrypt/letsencrypt-auto plugins
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Create a directory for the client to use for authorization:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;mkdir -p /srv/www/letsencrypt
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Then I put this into my nginx site config:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;vim /etc/nginx/sites-enabled/example.com
&lt;/code&gt;&lt;/pre&gt;

&lt;pre&gt;&lt;code&gt;location /.well-known/acme-challenge {
    root /srv/www/letsencrypt;
}
&lt;/code&gt;&lt;/pre&gt;

&lt;pre&gt;&lt;code&gt;service nginx reload
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;That allows the letsencrypt client to manage authorization files for my domain. And now I can generate the first certificate:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;/opt/letsencrypt/env/bin/letsencrypt certonly --webroot -w /srv/www/letsencrypt/ -d example.com,www.example.com
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Hopefully, that should generate a certificate and I can put them into the HTTPS section of my nginx config:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;vim /etc/nginx/sites-enabled/example.com
&lt;/code&gt;&lt;/pre&gt;

&lt;pre&gt;&lt;code&gt;ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;
&lt;/code&gt;&lt;/pre&gt;

&lt;pre&gt;&lt;code&gt;service nginx reload
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;And for the main benefit, I can now set up a cron job like this, that will make sure my certificates stay up to date:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;10 20 * * * /opt/letsencrypt/env/bin/letsencrypt-renewer &amp;gt;/dev/null &amp;amp;&amp;amp; service nginx reload &amp;gt;/dev/null
&lt;/code&gt;&lt;/pre&gt;
</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2016/01/lets-encrypt-and-nginx/</guid>
			
			<pubDate>Thu, 28 Jan 2016 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Using Raspberry Pi as an Arduino (AVR) programmer</title>
			<link>http://oxygene.sk/2015/02/using-raspberry-pi-as-an-arduino-avr-programmer/</link>
			<comments>http://oxygene.sk/2015/02/using-raspberry-pi-as-an-arduino-avr-programmer/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I bought a cheap Arduino nano clone from eBay and after I plugging it in via
USB, I was getting only errors when trying to upload some code. I realized
it came without the bootloader installed. And I didn&#39;t have a normal AVR ISP
programmer to program the flash memory on the microcontroller.&lt;/p&gt;

&lt;p&gt;The ATmega328P chip can be programmed using the SPI protocol. The process
requires 4 digital wires and Raspberry Pi with its GPIO pins
can be easily used to do that. On Linux, you can interact with GPIO pins
using a sysfs interface and avrdude (the application that can program the chip)
can talk the SPI protocol over this interface. So it&#39;s just a
matter of connecting the pieces together. Normally you would use
the AVR ISP pin header on Arduino to do this, but I did not have the
right connector for that, so I used regular pins and connect them on a
breadboard.&lt;/p&gt;

&lt;p&gt;You need to use specific pins on the Arduino, but you can use
any of the available pins on the Raspberry Pi. I used these:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;Pin D11 (MOSI) on Arduino to pin 20 on RPi&lt;/li&gt;
&lt;li&gt;Pin D12 (MISO) on Arduino to pin 16 on RPi&lt;/li&gt;
&lt;li&gt;Pin D13 (SCK) on Arduino to pin 21 on RPi&lt;/li&gt;
&lt;li&gt;Pin RESET on Arduino to pin 12 on RPi&lt;/li&gt;
&lt;li&gt;GND on Arduino to GND on RPi&lt;/li&gt;
&lt;li&gt;5V on Arduino to 3.3V on RPi&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Arduino should normally be powered by 5V, but Raspberry Pi
needs 3.3V on GPIO pins, so we will run the Arduino at
lower voltage. It should be fine for the programming.&lt;/p&gt;

&lt;p&gt;The result looked like this:&lt;/p&gt;

&lt;p&gt;&lt;a href=&quot;/uploads/rpi-arduino/IMG_20150202_145746.jpg&quot;&gt;
  &lt;img src=&quot;/uploads/rpi-arduino/IMG_20150202_145746-small.jpg&quot; /&gt;
&lt;/a&gt;
&lt;a href=&quot;/uploads/rpi-arduino/IMG_20150202_145722.jpg&quot;&gt;
  &lt;img src=&quot;/uploads/rpi-arduino/IMG_20150202_145722-small.jpg&quot; /&gt;
&lt;/a&gt;
&lt;a href=&quot;/uploads/rpi-arduino/IMG_20150202_145732.jpg&quot;&gt;
  &lt;img src=&quot;/uploads/rpi-arduino/IMG_20150202_145732-small.jpg&quot; /&gt;
&lt;/a&gt;&lt;/p&gt;

&lt;p&gt;Now the software part. The avrdude package that comes with Raspbian is not
compiled with linuxgpio support, so we need to build our own.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo apt-get install bison flex libusb-dev
cd /tmp
wget http://download.savannah.gnu.org/releases/avrdude/avrdude-6.1.tar.gz
tar xf avrdude-6.1.tar.gz
cd avrdude-6.1
./configure --prefix=/opt/avrdude --enable-linuxgpio
make
sudo make install
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;You need to edit &lt;code&gt;avrdude.conf&lt;/code&gt; to let it know the pins used.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo vim /opt/avrdude/etc/avrdude.conf
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Somewhere in the file find the commented out &lt;code&gt;linuxgpio&lt;/code&gt; section and add this in there.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;programmer
  id    = &quot;linuxgpio&quot;;
  desc  = &quot;Use the Linux sysfs interface to bitbang GPIO lines&quot;;
  type  = &quot;linuxgpio&quot;;
  reset = 12;
  sck   = 21;
  mosi  = 20;
  miso  = 16;
;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;If everything is connected and configured properly, you should be able to run this now and get into the avrdude console.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo /opt/avrdude/bin/avrdude -p atmega328p -c linuxgpio -v -t
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Now we need to modify the Arduino IDE configuration to recognize the new programmer. I don&#39;t know if this can be done locally, so we will edit the system-wide configuration file.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo vim /usr/share/arduino/hardware/arduino/programmers.txt
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Put this somewhere near the end of the file.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;rpigpio.name=RPi GPIO
rpigpio.protocol=linuxgpio
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;We also need to tell the Arduino IDE to use our version of avrdude. Again, because I don&#39;t know of any better solution, we will overwrite the default symlinks that come with the package.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo ln -fs /opt/avrdude/bin/avrdude /usr/share/arduino/hardware/tools/avrdude
sudo ln -fs /opt/avrdude/etc/avrdude.conf /usr/share/arduino/hardware/tools/avrdude.conf
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;That was the last configuration step and now we can actually program the chip.
Becuse you can&#39;t use the Linux GPIO sysft interface as a regular user, you need to start the Arduino IDE as root for this.
Select your board type in &quot;Tools -&gt; Board&quot;, select the &quot;RPi GPIO&quot; option in &quot;Tools -&gt; Programmer&quot; and then install the bootloader using &quot;Tools -&gt; Burn Bootloader&quot;.
Once the bootloader is installed you can close the IDE, remove the pin connections, plug in USB cable and start the IDE as a regular user.&lt;/p&gt;

&lt;p&gt;Hope that helps somebody.&lt;/p&gt;
</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2015/02/using-raspberry-pi-as-an-arduino-avr-programmer/</guid>
			
			<pubDate>Mon, 02 Feb 2015 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>"All Pull Requests" add-on for Atlassian Stash</title>
			<link>http://oxygene.sk/2013/09/all-pull-requests-add-on-for-atlassian-stash/</link>
			<comments>http://oxygene.sk/2013/09/all-pull-requests-add-on-for-atlassian-stash/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;Last few evenings I spend refreshing my Java skills and working on an add-on for
Atlasian Stash to display open pull requests from multiple repositories on a single place.&lt;/p&gt;

&lt;p&gt;Stash is the obvious choice for code hosting for a company using other Atlassian products,
but it&#39;s still very much in the beginnings and doesn&#39;t do much except for providing users
access to Git repositories. It supports pull requests, but if you use multiple repositories,
they kind of get &quot;lost&quot; because there are no good overview pages.&lt;/p&gt;

&lt;p&gt;This add-on adds two new pages, to view either all project-related pull requests or all
pull requests in the system, which makes it easy what&#39;s going on in the project.&lt;/p&gt;

&lt;p&gt;&lt;a href=&quot;https://marketplace.atlassian.com/plugins/sk.oxygene.stash.stash-all-pull-requests&quot;&gt;
  &lt;img src=&quot;/uploads/allpullrequests-small.png&quot; /&gt;
&lt;/a&gt;&lt;/p&gt;
</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2013/09/all-pull-requests-add-on-for-atlassian-stash/</guid>
			
			<pubDate>Thu, 26 Sep 2013 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Dropbox folder icon in GNOME 3</title>
			<link>http://oxygene.sk/2012/10/dropbox-folder-icon-in-gnome-3/</link>
			<comments>http://oxygene.sk/2012/10/dropbox-folder-icon-in-gnome-3/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I upgraded to GNOME 3 just over a week ago and when installing the
&lt;a href=&quot;http://tiheum.deviantart.com/art/Faenza-Icons-173323228&quot;&gt;Feanza icon theme&lt;/a&gt;, I noticed that there is a nice Dropbox folder
icon included. Nautilus allows you to set icon for any folder,
but only lets you select a specific image file, which doesn&#39;t look
particularly good when scaled.&lt;/p&gt;

&lt;p&gt;GNOME icon themes have icons in multiple sizes for each icon, so
rather then specific image files, it&#39;s better to use generic icon
names. I didn&#39;t find any GUI to assign an icon name to a folder,
but fortunately there is a way to do it using the
&lt;code&gt;gvfs-set-attribute&lt;/code&gt; tool:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;gvfs-set-attribute ~/Dropbox metadata::custom-icon-name folder-dropbox
&lt;/code&gt;&lt;/pre&gt;
</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2012/10/dropbox-folder-icon-in-gnome-3/</guid>
			
			<pubDate>Sat, 20 Oct 2012 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Twisted Trial and Jenkins</title>
			<link>http://oxygene.sk/2012/10/twisted-trial-and-jenkins/</link>
			<comments>http://oxygene.sk/2012/10/twisted-trial-and-jenkins/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;It&#39;s not completely obvious how to configure a Twisted-based job that uses &lt;a href=&quot;http://twistedmatrix.com/documents/current/core/howto/testing.html&quot;&gt;Trial&lt;/a&gt; for running tests in Jenkins, so hopefully this post will save somebody a little time in the future.&lt;/p&gt;

&lt;p&gt;Jenkins needs JUnit-style XML file to parse test results. You can get that output from Trial, if you pass the results thought &lt;a href=&quot;http://pypi.python.org/pypi/python-subunit&quot;&gt;subunit&lt;/a&gt; with &lt;a href=&quot;http://pypi.python.org/pypi/junitxml&quot;&gt;junitxml&lt;/a&gt;.&lt;/p&gt;

&lt;p&gt;Debian-based distributions have those two modules packaged, so you can install them with &lt;code&gt;apt-get&lt;/code&gt;.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;apt-get install python-subunit python-junitxml
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Elsewhere you can use &lt;code&gt;pip&lt;/code&gt; or &lt;code&gt;easy_install&lt;/code&gt;.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;pip install python-subunit junitxml
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Then in your Jenkins configuration, you can use the following command and let Jenkins know to collect the test results from &lt;code&gt;results.xml&lt;/code&gt;.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;trial --reporter=subunit MYPACKAGE | subunit-1to2  | subunit2junitxml &amp;gt;results.xml
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;&lt;em&gt;UPDATE: compatibility with the latest subunit&lt;/em&gt;&lt;/p&gt;
</description>
			
			<guid isPermaLink="true">http://oxygene.sk/2012/10/twisted-trial-and-jenkins/</guid>
			
			<pubDate>Wed, 17 Oct 2012 00:00:00 -0000</pubDate>
		</item>
		
		<item>
			<title>Goodbye Launchpad</title>
			<link>http://oxygene.sk/2011/04/goodbye-launchpad/</link>
			<comments>http://oxygene.sk/2011/04/goodbye-launchpad/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;All Acoustid components are now migrated to GitHub:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;a href=&quot;https://github.com/lalinsky/chromaprint&quot;&gt;Chromaprint&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://github.com/lalinsky/acoustid-fingerprinter&quot;&gt;Acoustid Fingerprinter&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://github.com/lalinsky/acoustid-server&quot;&gt;Acoustid Server&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://github.com/lalinsky/acoustid-index&quot;&gt;Acoustid Index&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;https://github.com/lalinsky/gst-chromaprint&quot;&gt;Chromaprint plug-in for GStreamer&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;Even though I &lt;em&gt;still&lt;/em&gt; prefer the idea of Bazaar, the tools built around Git and especially GitHub make it worth switching for me. I started using Bazaar a long time ago because it was the only VCS that was able to work on Windows back then. I built several tools to make it more comfortable for me, but I get the feeling the situation is not improving. Maybe Git really is the VCS we should all use...&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=1396</guid>
			
			<pubDate>Sat, 23 Apr 2011 13:13:22 -0000</pubDate>
		</item>
		
		<item>
			<title>Making Fail2ban with IPFW firewall on FreeBSD work</title>
			<link>http://oxygene.sk/2011/04/making-fail2ban-with-ipfw-firewall-on-freebsd-work/</link>
			<comments>http://oxygene.sk/2011/04/making-fail2ban-with-ipfw-firewall-on-freebsd-work/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;The internet is a nasty place, everybody is trying to hack into your servers if they are publicly accessible. Even though I always disable password authentication, so there is very little chance somebody could &quot;guess&quot; my private RSA key, I don&#39;t like &lt;code&gt;/var/log/auth.log&lt;/code&gt; being spammed. &lt;a href=&quot;http://www.fail2ban.org/wiki/index.php/Main_Page&quot;&gt;Fail2ban&lt;/a&gt; is a nice solution to that I use on Linux with iptables, but it was not working for me on FreeBSD with IPFW.&lt;/p&gt;

&lt;p&gt;My firewall is configured to drop everything by default, so it needs to have rules with connections that it should be allowed. However, the default IPFW ban action looks like this:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;actionban = ipfw add deny tcp from &amp;lt;ip&amp;gt; to &amp;lt;localhost&amp;gt; dst-port &amp;lt;port&amp;gt;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This inserts the rule at the end of the rule list, just before the default rule. This doesn&#39;t work for me, because it&#39;s after my own rule rule that allows traffic on that port from any address, so the newly added rule has no effect. I ended up modifying the IPFW action configuration file (&lt;code&gt;$PREFIX/etc/fail2ban/action.d/ipfw.conf&lt;/code&gt;) to dynamically generate rule numbers that are before my own rules:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;actionban = USEDNUMS=`ipfw list | perl -pe &#39;s/(\d{5}) .*\n/$1|/&#39; | perl -pe &#39;s/\|$//&#39;`
            NUM=`jot -w &#39;%%05d&#39; - &amp;lt;minnum&amp;gt; &amp;lt;maxnum&amp;gt; | grep -vE &quot;($USEDNUMS)&quot; | head -n1`
            ipfw add $NUM deny tcp from &amp;lt;ip&amp;gt; to &amp;lt;localhost&amp;gt; dst-port &amp;lt;port&amp;gt;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This only uses rule numbers between &lt;code&gt;&amp;lt;minnum&amp;gt;&lt;/code&gt; and &lt;code&gt;&amp;lt;maxnum&amp;gt;&lt;/code&gt;, selecting the first one that is available. I can set the variables in my &lt;code&gt;jails.conf&lt;/code&gt; configuration like this:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;[ssh-ipfw]

enabled  = true
filter   = sshd
action   = ipfw[localhost=any,minnum=01110,maxnum=01199]
logpath  = /var/log/auth.log
ignoreip = 168.192.0.1
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;The default unban action is also problematic. I was testing the above from a server that has some traffic on another port explicitly allowed. However, when canceling the SSH ban, Fail2ban deleted also my own rule because it was using only the IP for firewall rule matching. Changing it like this works for me, but it&#39;s still not ideal:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;actionunban = ipfw delete `ipfw list | grep -i &#39;deny tcp from &amp;lt;ip&amp;gt; to &amp;lt;localhost&amp;gt;&#39; | awk &#39;{print $1;}&#39;`
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Ideally I would like to check also the port numbers, but I can&#39;t find an easy way to resolve the service name to a port number.&lt;/p&gt;

&lt;p&gt;The final version of the IPFW Fail2ban action configuration file that I&#39;m using is &lt;a href=&quot;http://dl.dropbox.com/u/5215054/ipfw.conf&quot;&gt;here&lt;/a&gt;.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=1357</guid>
			
			<pubDate>Sat, 23 Apr 2011 12:58:56 -0000</pubDate>
		</item>
		
		<item>
			<title>MusicBrainz database replication</title>
			<link>http://oxygene.sk/2010/10/musicbrainz-database-replication/</link>
			<comments>http://oxygene.sk/2010/10/musicbrainz-database-replication/#disqus_thread</comments>
			
			<category>musicbrainz</category>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I wanted to work on the Acoustid lookup web service and for this I needed to setup a MusicBrainz replicated database slave. Normally I&#39;d use the Perl code from mb_server, but I didn&#39;t want to mess up the setup I have on the machine hosting &lt;a href=&quot;http://acoustid.org/&quot;&gt;acoustid.org&lt;/a&gt; with CPAN packages. I had a plan to write a non-Perl version of the replicated code earlier, but only yesterday I really needed it. It turned out to be easier than I expected, so I already have a working version that I use to update my local MB database. &lt;a href=&quot;https://github.com/lalinsky/mbslave&quot;&gt;Get it from GitHub&lt;/a&gt;, if you would like to give it a try.&lt;/p&gt;

&lt;p&gt;It has scripts for importing the database dumps as well as applying replication packets, so it can be used creating a MB database from scratch as well as updating an existing one. Instructions for setting up a new database can be found in the &lt;a href=&quot;https://github.com/lalinsky/mbslave/blob/master/README.txt&quot;&gt;README&lt;/a&gt; file.&lt;/p&gt;

&lt;p&gt;There are a few differences between these scripts and &lt;a href=&quot;http://bugs.musicbrainz.org/browser/mb_server/branches/RELEASE_20090524-BRANCH/admin&quot;&gt;mb_server&lt;/a&gt;:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;Depends only on Python and &lt;a href=&quot;http://initd.org/psycopg/&quot;&gt;psycopg2&lt;/a&gt;. Should be easy to set it up on non-Linux platforms, but I&#39;ve not tested that.&lt;/li&gt;
&lt;li&gt;Doesn&#39;t require additional space for extracting the database dump tarballs. It decompresses and imports the files on-the-fly.&lt;/li&gt;
&lt;li&gt;Can be configured to import the MB database into any schema, not just &quot;public&quot;.&lt;/li&gt;
&lt;li&gt;Can be configured to not replicate specific tables (e.g. direct search indexes, PUIDs).&lt;/li&gt;
&lt;li&gt;Regrouped transactions. The MB replication packet consists of multiple transactions (each of which has multiple database operations). The original replication code in mb_server imports the transactions exactly as they were executed in the master database. My code applies the whole packet in a single transaction. This is a small disadvantage, but from an external point of view (external scripts, triggers), there is no practical difference.&lt;/li&gt;
&lt;li&gt;New code, possibly buggy.&lt;/li&gt;
&lt;/ol&gt;


&lt;p&gt;The code currently assumes that you know how to setup the MusicBrainz database, so it doesn&#39;t do many checks. It it turns out it&#39;s useful also to somebody else, I can make it more robust.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=833</guid>
			
			<pubDate>Sat, 30 Oct 2010 13:06:17 -0000</pubDate>
		</item>
		
		<item>
			<title>Custom file associations on GNOME</title>
			<link>http://oxygene.sk/2010/09/custom-file-associations-on-gnome/</link>
			<comments>http://oxygene.sk/2010/09/custom-file-associations-on-gnome/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;It seems that there is no GUI method settings associations for custom files in GNOME. I&#39;ve had this problem before with &lt;a href=&quot;/projects/dbmodel/&quot;&gt;dbmodel&lt;/a&gt; before. It writes save its data in XML files that end with the extension &quot;.dmf&quot;. The problem is that GNOME&#39;s file associations work with MIME types and by default GNOME doesn&#39;t know anything about these silly .dmf files, it only sees XML files, so if I want to automatically open .dmf files with dbmodel, I can either set it for all XML files or for nothing. I eventually gave up on trying to do this, but since I started to use eMusic and switched to Chrome, it become much more annoying.&lt;/p&gt;

&lt;p&gt;The problem is that eMusic download files are also XML files, so I can&#39;t set the association just for &quot;.emx&quot; files. Another problem is that to open the files from Chrome, I can only use the default GNOME association. I can&#39;t use any other application without going to Nautilus and looking up the executable. The correct way to fix this seems to be to &lt;a href=&quot;http://library.gnome.org/devel/integration-guide/stable/mime.html.en&quot;&gt;add a new MIME type&lt;/a&gt;:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;mkdir -p ~/.local/share/mime/packages
vim ~/.local/share/mime/packages/emusic.xml
update-mime-database ~/.local/share/mime/
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Where the &lt;code&gt;emusic.xml&lt;/code&gt; file contains the following:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;&amp;lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&amp;gt;
    &amp;lt;mime-info xmlns=&quot;http://www.freedesktop.org/standards/shared-mime-info&quot;&amp;gt;
        &amp;lt;mime-type type=&quot;application/x-emusic&quot;&amp;gt;
        &amp;lt;comment&amp;gt;eMusic Download File&amp;lt;/comment&amp;gt;
        &amp;lt;glob pattern=&quot;*.emx&quot;/&amp;gt;
    &amp;lt;/mime-type&amp;gt;
&amp;lt;/mime-info&amp;gt;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;After running the commands, Nautilus identifies the .emx files as eMusic Download Files, so I can set to open them in the eMusic Download Manager, without touching the configuration for generic XML files.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=668</guid>
			
			<pubDate>Sun, 26 Sep 2010 13:21:13 -0000</pubDate>
		</item>
		
		<item>
			<title>Cross-compiling with CMake and Autotools</title>
			<link>http://oxygene.sk/2010/09/cross-compiling-with-cmake-and-autotools/</link>
			<comments>http://oxygene.sk/2010/09/cross-compiling-with-cmake-and-autotools/#disqus_thread</comments>
			
			<category>programming</category>
			
			<category>tools</category>
			
			<description>&lt;p&gt;[The last time]/2010/07/introducing-chromaprint/) I needed to build a Windows binary, I found it easier to cross-compile it from Linux than to setup a development environment on a freshly installed Windows machine. The problem is that even though it&#39;s not hard, you need to remember some obscure options. Today I needed to build a &lt;a href=&quot;http://forums.musicbrainz.org/viewtopic.php?pid=11111#p11111&quot;&gt;new version of ISRCsubmit&lt;/a&gt; and I had to search for the recipe again. So, let&#39;s document it somewhere I can find it in the future...&lt;/p&gt;

&lt;p&gt;First, you need the &lt;code&gt;mingw32&lt;/code&gt; package, which contains the compiler toolkit plus all the required WinAPI libraries. For cross-compiling CMake projects, you also need a file with paths to the MinGW toolkit:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_C_COMPILER i586-mingw32msvc-gcc)
set(CMAKE_CXX_COMPILER i586-mingw32msvc-g++)
set(CMAKE_FIND_ROOT_PATH /usr/i586-mingw32msvc /home/lukas/projects/win32build)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;I have a special directory for installing compiled binaries, which makes it easier to use previously compiled libraries, so that&#39;s included in the file. To compile a Windows version of &lt;a href=&quot;http://musicbrainz.org/doc/libmusicbrainz&quot;&gt;libmusicbrainz&lt;/a&gt;, which uses CMake, you can use a serie of command like this:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=/home/lukas/projects/mingw.cmake -DCMAKE_INSTALL_PREFIX=/home/lukas/projects/win32build/
make 
make install
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;To build libmusicbrainz I also needed &lt;a href=&quot;http://www.webdav.org/neon/&quot;&gt;neon&lt;/a&gt;, which uses autotools. Fortunately, cross-compiling autotools projects is usually also quite simple, but neon doesn&#39;t support MinGW out of the box, so &lt;a href=&quot;https://build.opensuse.org/package/view_file?file=neon-0.29.1-mingw.patch&amp;amp;package=mingw32-libneon&amp;amp;project=windows:mingw:win32&amp;amp;srcmd5=eb762d7b7894e4ebd1cda4f69a6b5c60&quot;&gt;one patch&lt;/a&gt; is necessary. After applying the patch, it can be compiled like this:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;./configure --host=i586-mingw32msvc --disable-debug --disable-webdav --prefix=/home/lukas/projects/win32build/
make
make install
&lt;/code&gt;&lt;/pre&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=657</guid>
			
			<pubDate>Wed, 15 Sep 2010 09:24:09 -0000</pubDate>
		</item>
		
	</channel>
</rss>

